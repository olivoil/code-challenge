MOCHA = ./node_modules/.bin/mocha

start:
	@NODE_PATH=lib ./bin/start

test:
	@NODE_PATH=lib ${MOCHA} --bail

lib/data.json:
	@./bin/update_db

.PHONY: start test
