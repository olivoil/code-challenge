
# Code Challenge

## Run

```
npm install
npm start
```

## Test

```
npm test
```

## Questions

* Was 3 days / 8 hours enough time? Why or why not.

  Yes. The scope was simple enough.

* How many hours would you estimate you spend total on the project?

  4 hours.

* Did you encounter any struggles while completing this? How did you overcome them?

  Yes, the main challenges was the string distance algorithm and the query by proximity to certain coordinates.

  I tried several string distance algorithms (levenshtein, Jaro Winkler, Dice...),
    but was not getting the right results. I found a project by Google called "Diff, Match, and Patch",
    which led me to a javascript implementation called "fuse.js". The results were satisfying then.

  I also read a few resources on geo hashing and how it can be used to query points within a certain radius.
  I found the npm module "node-geo-proximity", even though not exactly what I needed, helped to get something out fast.


* On a scale of 1-10, rate the complexity of the project (1 being super easy and 10 being the hardest thing you've ever done)

  Probably 2 or 3, but there's a lot of opportunity to make it more complicated!
  Having to implement the string distance algorithm and the geo-proximity one from scratch would definitely makes things a bit more challenging for instance.

  As far as small code challenges go, this one was actually well done and interesting!
