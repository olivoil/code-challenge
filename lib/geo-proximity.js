/**
 * Extracted from npm module node-geo-proximity.
 *
 * Copyright (c) 2014, Arjun Mehta.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

var geohash = require('ngeohash');

/**
 * Range-Radius Index
 *
 * This index is a list of ranges that correspond to the accuracy associated with
 * a particular bitDepth in reverse order from 52 bits. ie. rangeIndex[0] represents
 * 52 bits and an accuracy of a 0.5971m radius, while rangeIndex[7] represents 38 bits (52-(7*2))
 * and 76.4378m radius accuracy etc.
 *
 */
var rangeIndex = [
  0.6,            //52
  1,              //50
  2.19,           //48
  4.57,           //46
  9.34,           //44
  14.4,           //42
  33.18,          //40
  62.1,           //38
  128.55,         //36
  252.9,          //34
  510.02,         //32
  1015.8,         //30
  2236.5,         //28
  3866.9,         //26
  8749.7,         //24
  15664,          //22
  33163.5,        //20
  72226.3,        //18
  150350,         //16
  306600,         //14
  474640,         //12
  1099600,        //10
  2349600,        //8
  4849600,        //6
  10018863        //4
];

/**
 * Get Radius Bit Depth
 *
 * Returns a geohash integer bitDepth associated with a specific radius value in meters.
 *
 * @param {Number} radius
 * @returns {Number|null} bitDepth
 */
exports.rangeDepth = function rangeDepth(radius){
  for(var i=0; i < rangeIndex.length-1; i++){
    if(radius - rangeIndex[i] < rangeIndex[i+1] - radius){
      return 52-(i*2);
    }
  }
  return 2;
};

/**
 * Nearby Hash Ranges by Resolution
 *
 * Returns a set of optimum ranges at bitDepth that contain all geohashes within range of the passed in coordinate values at the radiusBitDepth.
 *
 * @param {Number} lat
 * @param {Number} lon
 * @param {Number} radiusBitDepth (determines the range)
 * @param {Number} bitDepth (bit depth of final hash values)
 * @returns {Hash Integer Ranges} Array
 */
exports.getQueryRangesFromBitDepth = function getQueryRangesFromBitDepth(lat, lon, radiusBitDepth, bitDepth){

  bitDepth = bitDepth || 52;
  radiusBitDepth = radiusBitDepth || 48;

  var bitDiff = bitDepth - radiusBitDepth;
  if(bitDiff < 0){
    throw "bitDepth must be high enough to calculate range within radius";
  }

  var i;
  var ranges = [],
      range;

  var lowerRange = 0,
      upperRange = 0;

  var hash = geohash.encode_int(lat, lon, radiusBitDepth);
  var neighbors = geohash.neighbors_int(hash, radiusBitDepth);

  neighbors.push(hash);
  neighbors.sort();

  for(i=0; i<neighbors.length; i++){
    lowerRange = neighbors[i];
    upperRange = lowerRange + 1;
    while(neighbors[i+1] === upperRange){
      neighbors.shift();
      upperRange = neighbors[i]+1;
    }
    ranges.push([lowerRange, upperRange]);
  }

  for(i=0; i<ranges.length; i++){
    range = ranges[i];
    range[0] = leftShift(range[0], bitDiff);
    range[1] = leftShift(range[1], bitDiff);
  }

  return ranges;
};

function leftShift(integer, shft){
  return integer * Math.pow(2, shft);
};
