
var http = require('http');
var parse = require('url').parse;
var search = require('search');

/**
 * Expose http server.
 */
var server = module.exports = http.Server(handler);

/**
 * http handler function.
 *
 * As the code challenge is very small,
 * I chose to keep the code simple and use simple standard library modules.
 *
 * @param {http.ClientRequest} req
 * @param {http.ServerResponse} res
 * @api public
 */
function handler(req, res){
  var url = parse(req.url, true);

  if (url.pathname !== "/suggestions"){
    res.writeHead(404, {"Content-Type": "application/json"});
    res.write(JSON.stringify({error: "404 Not Found"}));
    res.end();
    return;
  }

  search(url.query, function(err, results){
    res.writeHead(200, {"Content-Type": "application/json"});
    res.write(JSON.stringify(results));
    res.end();
    return;
  });
};
