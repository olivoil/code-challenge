
var db = require('data.json');
var Fuse = require('fuse.js');
var geo = require('geo-proximity');

/**
 * Expose `search`.
 */
module.exports = search;

/**
 * Search for a location by term,
 * optionally filter for locations close to a lat, lng.
 *
 * @param {Object} query
 * @param {String} query.q
 * @param {Number} query.latitude
 * @param {Number} query.longitude
 * @api public
 */
function search(query, cb){

  // simulate real async IO
  process.nextTick(function(){
    if (!query.q) return cb(null, []);

    var locations = db;

    if (query.latitude && query.longitude){
      locations = filterByLatLng(locations, query.latitude, query.longitude, 1000);
    }

    locations = filterByStringDistance(locations, query.q);

    cb(null, locations);
  });
}

/**
 * Filter locations to points within a certain `radius`
 * of the `latitude`, `longitude` coordinates.
 *
 * @param {Array} locations
 * @param {Number} latitude
 * @param {Number} longitude
 * @param {Number} radius in meters
 * @returns {Array}
 * @api private
 */
function filterByLatLng(locations, latitude, longitude, radius){
  var radiusBitDepth = geo.rangeDepth(radius);
  var ranges = geo.getQueryRangesFromBitDepth(latitude, longitude, radiusBitDepth, 52);

  return ranges.reduce(function(res, range){
    return res.concat(locations.filter(function(item){
      return item.geo_hash >= range[0] && item.geo_hash <= range[1];
    }));
  }, []);
}

/**
 * Filter and order locations by the proximity of their name with `term`.
 *
 * @param {Array} locations
 * @param {String} term
 * @returns {Array}
 * @api private
 */
function filterByStringDistance(locations, term){
  locations = locations || db;

  var fuse = new Fuse(locations, {
    caseSensitive: false,
    includeScore: true,
    shouldSort: true,
    threshold: 0.5,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    keys: ["name"]
  });

  var results = fuse.search(term).reduce(function(ret, res){
    var obj = res.item;
    obj.score = 1 - res.score;
    ret.push(obj);
    return ret;
  }, []);

  return results;
}
