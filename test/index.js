
var assert = require('assert');
var request = require('supertest');
var app = require('.');

describe("/suggestions", function(){
  it("returns an empty array if `q` is omitted", function(){
    request(app)
    .get("/suggestions?q=")
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function(err, res){
      assert.ifError(err);
      assert(res.body);
      assert(res.body instanceof Array);
      assert(0 == res.body.length);
    });
  });

  it("returns results with `q`", function(){
    request(app)
    .get("/suggestions?q=Ric")
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function(err, res){
      assert.ifError(err);
      assert(res.body);
      assert(res.body.length > 3);
    });
  });

  it("returns results with `q` and a location", function(){
    request(app)
    .get("/suggestions?q=Ric&latitude=32.83&longitude=-97.22")
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function(err, res){
      assert.ifError(err);
      assert(res.body);
      assert.equal(1, res.body.length);
      assert.equal("North Richland Hills", res.body[0].name);
    });
  });
});

describe("unknown path", function(){
  it("returns a 404 for paths others than '/suggestions'", function(){
    request(app)
    .get("/unknown_path")
    .expect('Content-Type', /json/)
    .expect(404)
    .end(function(err, res){
      assert.ifError(err);
    });
  });
});
