
var search = require('search');
var assert = require('assert');
var debug = require('debug')('test');

describe('search(query, cb)', function(){

  it("returns an empty array if `query.q` is omitted", function(done){
    search({}, function(err, res){
      assert.ifError(err);
      assert.equal(0, res.length);
      done();
    });
  });

  it("returns an empty array if there is no match", function(done){
    search({q: "Something Random"}, function(err, res){
      log(res, "no match");
      assert.ifError(err);
      assert.equal(0, res.length);
      done();
    });
  });

  it("returns some results for an exact match", function(done){
    search({q: "Richland Hills"}, function(err, res){
      log(res, "exact match");
      assert.ifError(err);
      assert(res.length);
      assert.equal(1, res[0].score);
      done();
    });
  });

  it("returns some results for `Ric`", function(done){
    search({q: "Ric"}, function(err, res){
      log(res, "partial match");
      assert.ifError(err);
      assert(res.length > 3);
      done();
    });
  });

  it("sorts results", function(done){
    search({q: "city"}, function(err, res){
      log(res, "sorted results");
      assert.ifError(err);

      var scores = res.map(function(item){ return item.score; });
      for (var i = 0; i < scores.length; i++){
        if (i) assert(scores[i - 1] >= scores[i]);
      }
      done();
    });
  });

  it("limits the results when a location is given", function(done){
    search({q: "Ric", latitude: 32.83, longitude: -97.22}, function(err, res){
      log(res, "filtered by location");
      assert.ifError(err);

      assert.equal(1, res.length);
      assert.equal("North Richland Hills", res[0].name);
      done();
    });
  });

});

function log(res, name){
  var ex = res.slice(0,10).reduce(function(ret, item){
    ret.push({name: item.name, score: item.score});
    return ret;
  }, []);

  debug(ex, name, res.length);
}
